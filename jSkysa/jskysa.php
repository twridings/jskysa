<?php
/**
 * A Joomla 1.7 plugin to include the Skysa App Bar (http://www.skysa.com) in your Joomla website.
 * Automatically adds the Skysa bar without needing to modify your template or create a custom module
 * Requires PHP 5+
 * 
 * NOTE: This is NOT an official Skysa product.  If you have problems with this plugin, please do not
 * contact Skysa as they will not be able to help you (unless its a problem with your actual Skysa account).
 * 
 * Usage:
 * 1) Install the plugin via the admin panel
 * 2) Edit the plugin and enter your Skysa App Bar Id (can be found in your Skysa account details)
 * 3) Enable the plugin
 * 
 * @package		jSkysa
 * @subpackage	System
 * @author		T.W. Ridings <travis@twridings.com>
 * @version		1.2
 * @license		GNU/GPL, see LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See LICENSE for more details.
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.log.log');

class plgSystemjskysa extends JPlugin {
    private $pluginParams;
	private $appBarId;
    private $isFrontend;

	/**
	 * Constructor
	 * 
	 * @access public
	 * @param object The object to observe -- event dispatcher.
	 * @param object The configuration object for the plugin.
	 * @return void
	 */
    public function plgSystemjskysa(&$subject, $config) {
        parent::__construct($subject);
		
		// Abort if this isn't, at least, PHP5 to avoid errors
		if(defined('PHP_VERSION')) {
			$version = PHP_VERSION;
		} elseif(function_exists('phpversion')) {
			$version = phpversion();
		} else {
			$version = '0';
		}
		if(!version_compare($version, '5.0.0', '>=')) return;
		
		//Setup the logger.
		try {
			JLog::addLogger(array('text_file' => 'plg_jskysa_log.php'), JLog::ALL, array('jskysa'));
		} catch(Exception $e) {
			// nothing to do here
		}
		
        //Load the plugin parameters.
        try {
	        $plugin = JPluginHelper::getPlugin('system', 'jskysa'); 
			$this->pluginParams = new JRegistry(); 
			$this->pluginParams->loadJSON($plugin->params);
			$this->appBarId = $this->pluginParams->get('app_bar_id');
			$this->isFrontend = JFactory::getApplication()->isSite();
		} catch(Exception $e) {
			JLog::add('[[' . __METHOD__ . ']] Error: Could not load plugin params - ' . $e->getMessage(), JLog::ERROR, 'jskysa');
		}
    }

	/**
	 * Inject the Skysa code into the page just before the closing body tag
	 * Only included on the front end
	 * 
	 * @access public
	 * @link http://docs.joomla.org/Plugin/Events/System#onAfterRender
	 * @return void
	 */
    public function onAfterRender() {
    	if($this->isFrontend && !empty($this->appBarId)) {
    		try {
	        	$output = JResponse::getBody();
            	$endbody = '/<\/body>/';
            	$skysacode = "<a href='http://www.sksysa.com' id='SKYSA-NoScript'>Website Apps</a><script type='text/javascript' src='http://static2.skysa.com?i=".$this->appBarId."' async='true'></script></body>";
            	$output = preg_replace($endbody, $skysacode, $output, 1);
	        	JResponse::setBody($output);
			} catch(Exception $e) {
				JLog::add('[[' . __METHOD__ . ']] Error: Failed to inject Skysa App Bar code - ' . $e->getMessage(), JLog::ERROR, 'jskysa');
			}
		}
    }
}